//
//  main.m
//  MyPod
//
//  Created by amber on 05/25/2017.
//  Copyright (c) 2017 amber. All rights reserved.
//

@import UIKit;
#import "MPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MPAppDelegate class]));
    }
}
