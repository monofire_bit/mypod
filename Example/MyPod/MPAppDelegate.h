//
//  MPAppDelegate.h
//  MyPod
//
//  Created by amber on 05/25/2017.
//  Copyright (c) 2017 amber. All rights reserved.
//

@import UIKit;

@interface MPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
